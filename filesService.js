// requires...
const fs = require('fs');
const fsPromise = require('fs/promises');
const path = require('path');
const filesFolder = path.resolve('./files');
const extensions = ['txt', 'json', 'xml', 'js', 'log', 'yaml'];

// constants...

function createFile (req, res, next) {
  // Your code to create the file.
  try {
    const { filename, content } = req.body;

    if (filename === undefined) {
      res.status(400).json({message: 'Please specify "filename" parameter'});
      return;
    }
    if (content === undefined) {
      res.status(400).json({message: 'Please specify "content" parameter'});
      return;
    }
    const fileExtension = path.extname(filename).split('.')[1];

    if (extensions.includes(fileExtension)) {
      fs.writeFileSync(path.join(filesFolder, filename), content, { flag: 'a+' });
      res.status(200).send({ "message": "File created successfully" });
    } else {
      res.status(400).json({
        message: `File extension should be one of the followings: ${extensions}`
      });
    }

  } catch (error) {
    console.error(error.message);
    res.status(500).json({message: 'Server error'});
  }
}

function getFiles (req, res, next) {
  // Your code to get all files.
  const filesList = [];
  try {
    fs.readdir(filesFolder, (err, result) => {
      if (err) {
        res.status(400).json({message: 'Client error'});
      }
      result.forEach(file => {
        filesList.push(file);
      })
      res.status(200).send({
        "message": "Success",
        "files": filesList});
    })
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
}

const getFile = async (req, res, next) => {
  // Your code to get all files.
  try {
    let uploadedDate;
    let extension = path.extname(req.url).split('.')[1];
    fs.stat(filesFolder + req.url, (error, stats) => {
      if (error) {
        console.log(error);
        return;
      }
      uploadedDate = stats.birthtime;
    });
    res.status(200).send({
      "message": "Success",
      "filename": path.basename(req.url),
      "content": await fsPromise.readFile(filesFolder + req.url, { encoding: 'utf8' }),
      "extension": extension,
      "uploadedDate": uploadedDate});
  } catch (error) {
    res.status(400).json({message: 'Server error'});
  }
}

// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile
}
